from bs4 import BeautifulSoup
import requests
import unidecode

URL = "http://um.edu.ar/es/fi/carreras/ingenieria-en-electronica.html"

questions = []
responses = []

# Realizamos la petición a la web
req = requests.get(URL)

# Comprobamos que la petición nos devuelve un Status Code = 200
status_code = req.status_code

# Si trae los datos del html realiza las tareas del scrapper
if status_code == 200:

    # Pasamos el contenido HTML de la web a un objeto BeautifulSoup()
    html = BeautifulSoup(req.text, "html.parser")
    # Obtenemos todos los divs donde están las entradas
    entradas = html.find_all('div', {'class': 'contenido'})
    # Obtenemos todos los parrafos
    content = html.find_all('p')
    # Obtenemos todos los header de h2
    titles1 = html.find_all('h2')
    # Obtenemos todos los header de h3
    titles2 = html.find_all('h3')

    # Se itera para cada parrafo
    for p in content:
        # Si se encuentra la palabra duracion en el parrafo se realiza lo de abajo
        if "Duracion" in p.prettify().lower() or "duración" in p.prettify().lower():
            # Se obtiene el texto limpio sin tags
            text = p.get_text()
            formatted_text = unidecode.unidecode(text[text.index("Duración"):].replace(':', ''))

            # Se elimina lo anterior a duracion, se reemplazan los dos puntos y caracteres especiales.
            formatted_text = unidecode.unidecode(formatted_text.replace(':', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\t', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\r', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\n', ''))
            formatted_text = formatted_text.strip()

            # Se añade la pregunta a la cual se debe responder
            questions.append("duracion de ingenieria electronica")
            responses.append(formatted_text)
            questions.append("cuanto dura ingenieria electronica?")
            responses.append(formatted_text)
            questions.append("cuantos anos dura la carrera ingenieria en electronica?")
            responses.append(formatted_text)

        if "La facultad posee" in p.prettify().lower() or "la facultad posee" in p.prettify().lower():
            # Se obtiene el texto limpio sin tags
            text = p.get_text()

            # Se elimina lo anterior a duracion, se reemplazan los dos puntos y caracteres especiales.
            formatted_text = unidecode.unidecode(text.replace(':', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\t', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\r', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\n', ''))
            formatted_text = formatted_text.strip()

            # Se añade la pregunta a la cual se debe responder
            questions.append("Que posee la facultad")
            responses.append(formatted_text)
            questions.append("La facultad que posee")
            responses.append(formatted_text)

        if "Un ingeniero electrónico no solo maneja" in p.prettify().lower() or "un ingeniero electrónico no solo maneja" in p.prettify().lower():
            # Se obtiene el texto limpio sin tags
            text = p.get_text()
            # Se elimina lo anterior a duracion, se reemplazan los dos puntos y caracteres especiales.
            formatted_text = unidecode.unidecode(text.replace(':', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\t', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\r', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\n', ''))
            formatted_text = formatted_text.strip()

            # Se añade la pregunta a la cual se debe responder
            questions.append("Que estudia el ingeniero en electronica")
            responses.append(formatted_text)
            questions.append("el ingeniero en electronica que estudia?")
            responses.append(formatted_text)
            questions.append("que estudia el ing en electronica")
            responses.append(formatted_text)

        if "Algunos ejemplos del campo" in p.prettify().lower() or "algunos ejemplos del campo" in p.prettify().lower():
            # Se obtiene el texto limpio sin tags
            text = p.get_text()
            # Se elimina lo anterior a duracion, se reemplazan los dos puntos y caracteres especiales.
            formatted_text = unidecode.unidecode(text.replace(':', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\t', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\r', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\n', ''))
            formatted_text = formatted_text.strip()

            # Se añade la pregunta a la cual se debe responder
            questions.append("En que se desempena un ingeniero en electronica")
            responses.append(formatted_text)
            questions.append("que hace un ingeniero en electronica")
            responses.append(formatted_text)

        if "La carrera de Ingeniería Electrónica está orientada" in p.prettify().lower() or "la carrera de Ingeniería Electrónica está orientada" in p.prettify().lower():
            # Se obtiene el texto limpio sin tags
            text = p.get_text()
            # Se elimina lo anterior a duracion, se reemplazan los dos puntos y caracteres especiales.
            formatted_text = unidecode.unidecode(text.replace(':', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\t', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\r', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\n', ''))
            formatted_text = formatted_text.strip()

            # Se añade la pregunta a la cual se debe responder
            questions.append("En que esta orientada la carrera")
            responses.append(formatted_text)
            questions.append("la carrera de ingeniero electronico a que esta orientada")
            responses.append(formatted_text)
            questions.append("la carrera a que esta orientada")
            responses.append(formatted_text)

        if "SISTEMAS DE REPRESENTAC" in p.prettify():
            # Se obtiene el texto limpio sin tags
            text = p.get_text()
            # Se elimina lo anterior a duracion, se reemplazan los dos puntos y caracteres especiales.
            formatted_text = unidecode.unidecode(text.replace(':', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\t', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\r', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\n', ''))
            formatted_text = formatted_text.strip()

            # Se añade la pregunta a la cual se debe responder
            questions.append("que materias tienen ingenieria en electronica")
            responses.append(formatted_text)
            questions.append("cuales son las materias de ingenieria en electronica")
            responses.append(formatted_text)

    # Se abre el archivo en modo escritura
    file = open("./data/electronica.yml", "w+")
    # Se escriben las primeras lineas del yaml necesarias para el ChatBot
    file.write("categories: \n- electronica \nconversations: \n")

    # Para cada pregunta se asocia una respuesta
    for i in range(len(responses)):
        file.write("- - " + questions[i] + "\n")
        file.write("  - " + responses[i] + "\n")

# En caso de que la pagina no devuelva un codigo 200 (Exito) se va a el error
else:
    print("Status Code %d" % status_code)