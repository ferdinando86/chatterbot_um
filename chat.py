# -*- coding: utf-8 -*-
import logging
import sys
from chatterbot import ChatBot
from chatterbot.conversation import Statement
from chatterbot.trainers import ListTrainer, ChatterBotCorpusTrainer


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


logging.basicConfig(level=logging.CRITICAL)

chat = ChatBot(  # se declara la entidad del chat, nueva instancia del bot
    'chatterbot',  # nombre del bot
    storage_adapter='chatterbot.storage.SQLStorageAdapter',  # adaptador para el almacenamiento en SQLite
    # database='./data/database.sqlite',                       # ubicacion del archivo SQLite donde se guardan los datos
    trainer='chatterbot.trainers.ListTrainer',  # adaptador logico para el entrenamiento
    # logic_adapters=[{                                        # clase que devuelve respuesta
    # "import_path": "chatterbot.logic.BestMatch",
    # "statement_comparison_function": "chatterbot.comparisons.levenshtein_distance",
    # "response_selection_method": "chatterbot.response_selection.get_most_frequent_response"
    # }]
)

trainer = ListTrainer(chat)
initialtrainer = ChatterBotCorpusTrainer(chat)


def get_feedback():
    text = input()
    if 'si' in text.lower():
        return True
    elif 'no' in text.lower():
        return False
    else:
        print('Por favor escriba "Si" or "No"')
        return get_feedback()


# Lista donde se guardan las preguntas y respuestas para entrenar
last_input_statement = []

print('Escribe algo para empezar: ')

# The following loop will execute each time the user enters input
while True:
    try:
        # Se carga la pregunta del usuario
        input_statement = Statement(text=input())

        # Busca la respuesta
        response = chat.generate_response(input_statement)

        print(f"{bcolors.OKBLUE}")
        print("{}".format(
            response.text
        ))
        print(f"{bcolors.ENDC}")

        if len(sys.argv) > 1:
            if sys.argv[1] == "-t" or sys.argv[1] == "--train":
                # Solicitar feedback
                print(f"{bcolors.WARNING}Es esto una respuesta coherente?{bcolors.ENDC}")
                response = response.text

                # Si la respuesta es incorrecta
                if get_feedback() is False:
                    print(f"{bcolors.WARNING}Por favor escriba la respuesta correcta: {bcolors.ENDC}")
                    # Cargar respuesta brindada por el usuario
                    response = input()
                    print("Respuesta agregada al Chatbot")

            # Cargar en la lista la respuesta
            last_input_statement.append(input_statement.text)
            last_input_statement.append(response)
            # Entranamiento del modelo con los nuevos valores
            trainer.train(last_input_statement)
            # Limpia la lista
            last_input_statement.clear()
            print("Escribe algo:")

    # Press ctrl-c or ctrl-d on the keyboard to exit
    except (KeyboardInterrupt, EOFError, SystemExit):
        break
