# Chatbot de información

Este repositorio contiene el proyecto realizado por los alumnos de la cátedra de Inteligencia Artificial de la Facultad de Ingeniería de la Universidad de Mendoza, Sede San Rafael.

## Objetivo 

Realizar un chatbot que permita a los usuarios realizar consultas y preguntas sobre diferentes aspectos del funcionamiento de la Universidad y las carreras que ofrece.

## Librerías utilizadas

	- Chatterbot

## Integrantes
	
	Alumnos :
		- Julian Ariel Campana @J.Campana
		- Ornella Mónica Grasso Viola @ornee.gv	
		- Jonatan Kondratiuk @JonatanKon
		- Fernando Simón López @ferdinando86
		- Matias Nicolas Romani @matias225
		- Marcos Rugoso @rugosomarcosss
		
	Profesora : Ing. Andrea Navarro Moreno

## Instalación

Para hacer uso del proyecto de Chatterbot se requerira tener instalada una version de Python > 3.3 y correr el script install.sh.

## Utilización

